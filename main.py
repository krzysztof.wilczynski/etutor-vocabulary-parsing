import argparse
from bs4 import BeautifulSoup
import os
import re
import wget


def main(file):
    with open(file, "r", encoding="utf-8") as SF:
        vocabulary = SF.read().split('<p class=\"hws phraseEntity\">')

    parsed_words = []

    try:
        os.chdir("audio")
    except OSError:
        os.mkdir("audio")
        os.chdir("audio")

    count = 1

    for raw in vocabulary[1:]:
        print(str(round(100 / len(vocabulary[1:]) * count, 2)) + "% | " + str(count) + "/" + str(len(vocabulary[1:])))
        soup = BeautifulSoup(raw, "html.parser")
        raw_english = soup.find("span", class_="hw").get_text().strip().replace(u'\xa0', u' ').replace(
            'American English', 'AE').replace('British English', 'BE')
        raw_polish = re.search(r'</span>\s*=\s*([^<&]+)', raw).groups()[0].strip()

        try:
            polish_sentence = soup.find("span", class_="sentenceTranslation").get_text().strip()
        except AttributeError:
            polish_sentence = ''

        try:
            english_sentence = soup.find("ul", class_="sentencesul").get_text().strip()
            english_sentence = re.search(r'(.*)\n', english_sentence).groups()[0].strip()
        except AttributeError:
            english_sentence = ''

        english_audio = soup.find("span", class_="soundOnClick")['data-audio-url']
        if english_audio:
            url = "https://www.etutor.pl" + english_audio
            wget.download(url)

        parsed_word = {
            'english': raw_english,
            'polish': raw_polish,
            'english_sentence': english_sentence,
            'polish_sentence': polish_sentence,
            'english_audio': "[sound:" + english_audio.split('/')[-1] + "]"
        }

        parsed_words.append(parsed_word)
        count += 1

    os.chdir('..')
    new_file = open("anki_export.txt", "w")
    sentences_file = open("lwt_export.txt", "w")
    for word in parsed_words:
        new_file.write(
            word['polish'] + ";" + word['english'] + ';' + word['english_sentence'] + ';' + word[
                'polish_sentence'] + ';' + word['english_audio'] + '\n')
        sentences_file.write(word['english_sentence'] + '\n')
    new_file.close()
    sentences_file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Przygotuj plik z kodem źródłowym ze strony słówek na eTutor")
    parser.add_argument("-f", "--file", help="Plik z kodem, html lub txt", required=True)
    args = parser.parse_args()
    main(args.file)
